app.controller('SubjectsController',
  function (Subjects, Roomtypes) {
    this.subjects = Subjects.query();
    this.roomtypes = Roomtypes.query();

    this.save = function (subject) {
      subject.roomtype = subject.roomtype.type;
      console.log(subject);
      Subjects.save(subject);
    }

    this.remove = function (subject) {
      Subjects.remove(subject);
    }
  }
);
