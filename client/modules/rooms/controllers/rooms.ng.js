app.controller('RoomsController',
  function (Rooms, Roomtypes) {
    this.rooms = Rooms.query();
    this.roomtypes = Roomtypes.query();
  
    this.save = function (room) {
      Rooms.save(room);
    }

    this.saveType = function (roomtype) {
      Roomtypes.save(roomtype);
    }

    this.remove = function (room) {
      Room.remove(room);
    }

    this.removeType = function (roomtype) {
      Roomtypes.remove(roomtype);
    }
  }
);
