app.filter('weekNumToDay',
  function () {
    var days = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
    return function (weekNum) {
      return days[weekNum - 1];
    }
  }
);
