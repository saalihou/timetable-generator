app.controller('TimeslotsController',
  function (Timeslots, Timeslotsets, $meteor) {
    this.timeslots = Timeslots.query();
    this.timeslotsets = Timeslotsets.query();

    this.save = function (timeslot) {
      Timeslots.save(timeslot);
    }

    this.saveSet = function (set) {
      Timeslotsets.save(set);
    }

    this.remove = function (timeslot) {
      Timeslots.remove(timeslot);
    }

    this.removeSet = function (set) {
      Timeslotsets.remove(set);
      var _ = lodash;
      var childSlots = _.where(this.timeslots, {parentSet: set.name});
      this.remove(childSlots);
    }

    this.duplicate = function (srcSet, destSet) {
      $meteor.call('duplicateTimeslots', srcSet, destSet);
    }
  }
);
