app.controller('TimeslotsUIController',
  function ($scope, Timeslotsets, $meteor) {

    this.days = [1, 2, 3, 4, 5, 6];
    this.hours = [];
    for (var i = 8; i <= 19; i++) {
      this.hours.push(i);
    }
    this.minutes = [0, 30];
    this.durations = [1, 2];

    function afterSets() {
      this.activeSet = Timeslotsets.query()[0];
      this.activeSetType = this.activeSet.type;
    }

    $scope.$meteorSubscribe('timeslotsets').then(angular.bind(this, afterSets));

    $scope.$watch(
      angular.bind(this, function () {
        return this.activeSet;
      }),
      angular.bind(this, function (activeSet) {
        if (activeSet) {
          this.newTimeslot.parentSet = activeSet.name;
        }
      })
    )
    $scope.$watch(
      angular.bind(this, function () {
        return this.activeSetType;
      }),
      angular.bind(this, function (activeSetType) {
        if (activeSetType) {
          this.newSlotSet.type = activeSetType;
        }
      })
    )

    this.newTimeslot = {
      day: 1,
      hour: 8,
      minute: 0,
      duration: 2
    }
    this.newSlotSet = {}
  }
);
