app.controller('RoomsUIController',
  function ($scope, Roomtypes, $meteor) {

    function afterTypes() {
      this.activeType = Roomtypes.query()[0];
    }

    $scope.$meteorSubscribe('roomtypes').then(angular.bind(this, afterTypes));

    $scope.$watch(
      angular.bind(this, function () {
        return this.activeType;
      }),
      angular.bind(this, function (activeType) {
        if (activeType) {
          this.newRoom.type = activeType.type;
        }
      })
    )

    this.newRoom = {}
  }
);
