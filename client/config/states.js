app.config(function ($stateProvider) {
  $stateProvider
    .state('webapp', {
      templateUrl: 'client/webapp/template.ng.html',
      abstract: true
    })
    .state('webapp.timeslots', {
      templateUrl: 'client/webapp/timeslots/views/template.ng.html',
      url: '/timeslots'
    })
    .state('webapp.rooms', {
      templateUrl: 'client/webapp/rooms/views/template.ng.html',
      url: '/rooms'
    })
    .state('webapp.subjects', {
      templateUrl: 'client/webapp/subjects/views/template.ng.html',
      url: '/subjects'
    })
});
