app.service('CollectionResourceFactory',
  function ($meteor) {
    var CollectionResourceFactory = function (Collection) {
      var AngularCollection = $meteor.collection(Collection);
      return {
        query: function (selector) {
          return AngularCollection;
        },

        get: function (selector) {
          return $meteor.object(Collection, selector);
        },

        save: function (doc) {
          return AngularCollection.save(doc);
        },

        remove: function (selectorOrDoc) {
          return AngularCollection.remove(selectorOrDoc);
        }
      }
    }
    return CollectionResourceFactory;
  }
);
