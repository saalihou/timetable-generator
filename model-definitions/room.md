# Room

## name
The identifier already present in the school identifying system (different
from the database identifier)

## type
Foreign key on RoomType.type

## capacity
Integer representing the maximum number of students a room can receive
