# TimeSlot

## Day
Integer identifying the day of the week (1-6)

## Hour
Integer identifying the start hour (0-23)

## Minute
Integer identifying the start minute (0, 30)

## parent set
String identifying which set the timeslot belongs to
