# TimeSlotSet

## name
A string uniquely identifying a timeslot set

## type
The type of the timeslot. Whether the timeslot is attributed to a class
or a teacher. Enums: ['class', 'teacher']
