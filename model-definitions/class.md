# Class

# identifier
The identifier already present in the school identifying system (different
from the database identifier)

# human name
A string representing the name of the class

# students count
The number of students in the class

# subjects
Array of objects containing:
  - a foreign key on Subject.identifier
  - a hours count that represents the minimum number of hours the class should
  study the subject
  - a priority which indicates the priority of the subject to the class
  relative to other classes
