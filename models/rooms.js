Rooms = new Mongo.Collection('rooms');

var RoomSchema = ({
  name: {
    type: String,
    unique: true
  },
  capacity: {
    type: Number
  },
  type: {
    type: String,
    custom: DBHelpers.foreignKey('roomtypes', 'type', 'invalidType')
  }
});

Rooms.attachSchema(RoomSchema);
Collections['rooms'] = Rooms;
