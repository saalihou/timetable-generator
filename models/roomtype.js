Roomtypes = new Mongo.Collection('roomtypes');

var RoomtypeSchema = ({
  type: {
    type: String,
    unique: true
  }
});

Roomtypes.attachSchema(RoomtypeSchema);
Collections['roomtypes'] = Roomtypes;
