Timeslots = new Mongo.Collection('timeslots');

var TimeslotSchema = new SimpleSchema({
  day: {
    type: Number,
    min: 1,
    max: 6
  },
  hour: {
    type: Number,
    min: 0,
    max: 23
  },
  minute: {
    type: Number,
    allowedValues: [0, 30]
  },
  duration: {
    type: Number,
    allowedValues: [1, 2]
  },
  parentSet: {
    type: String,
    custom: DBHelpers.foreignKey('timeslotsets', 'name', 'invalidParentSet')
  }
});

// slot timing validation to prevent slot overlapping
TimeslotSchema.addValidator(function () {
  var day = this.field('day').value,
    hour = this.field('hour').value,
    minute = this.field('minute').value,
    duration = this.field('duration').value,
    parentSetName = this.field('parentSet').value;

  var newSlotStart = new Date();
  var newSlotEnd = new Date();
  // we set start seconds to 1 so that there is no overlapping
  // between two nearby slots
  newSlotStart.setDate(day);
  newSlotStart.setHours(hour, minute, 1, 0);
  newSlotEnd.setDate(day);
  newSlotEnd.setHours(hour + duration, minute, 0, 0);

  var hasTimingError = false;
  var existingSlots = Timeslots.find({parentSet: parentSetName});
  existingSlots.forEach(function (slot) {
    var slotStart = new Date();
    var slotEnd = new Date();
    slotStart.setDate(slot.day);
    slotStart.setHours(slot.hour, slot.minute, 1, 0);
    slotEnd.setDate(slot.day);
    slotEnd.setHours(slot.hour + slot.duration, slot.minute, 0, 0);

    var isBefore = (newSlotStart < slotStart && newSlotEnd < slotStart)
    var isAfter = (newSlotStart > slotEnd);
    if (!isBefore && !isAfter) {
      hasTimingError = true;
    }
  });

  if (hasTimingError) {
    return 'timingError';
  }
});

Timeslots.attachSchema(TimeslotSchema);
Collections['timeslots'] = Timeslots;
