Subjects = new Mongo.Collection('subjects');

var SubjectSchema = ({
  name: {
    type: String,
    unique: true
  },
  roomtype: {
    type: String,
    custom: DBHelpers.foreignKey('roomtypes', 'type', 'invalidRoomType')
  }
});

Subjects.attachSchema(SubjectSchema);
Collections['subjects'] = Subjects;
