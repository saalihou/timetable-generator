Timeslotsets = new Mongo.Collection('timeslotsets');

var TimeslotsetSchema = ({
  name: {
    type: String,
    unique: true
  },
  type: {
    type: String,
    allowedValues: ['class', 'teacher']
  }
});

Timeslotsets.attachSchema(TimeslotsetSchema);
Collections['timeslotsets'] = Timeslotsets;
