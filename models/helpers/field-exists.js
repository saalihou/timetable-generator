if (Meteor.isServer) {
  DBHelpers.fieldExists = function (collectionName, field, val) {
    var Collection = Collections[collectionName];
    var selector = {};
    selector[field] = val;
    var val = Collection.findOne(selector);
    return (val ? true : false);
  }
}
