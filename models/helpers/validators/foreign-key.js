DBHelpers.foreignKey = function (collectionName, key, errType) {
  var validator = function () {
    if (Meteor.isServer) {
      if (!DBHelpers.fieldExists(collectionName, key, this.value)) {
        return errType;
      }
      return;
    }
  }

  return validator;
}
