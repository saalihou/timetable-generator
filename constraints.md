# Time

## semester
The length of a semester MUST be at most 20 weeks.

## week
The week ranges from monday at 8am to saturday at 12pm.

## day
On mondays, tuesdays, thursdays and fridays classes cannot extend beyond the
time range hereafter: 8am-12pm, 2.30pm-6.30pm.
On wednesdays and saturdays, classes cannot extend beyond the time range
hereafter: 8am-12pm.

# Subjects
A subject CAN be studied by 0 or more classes.
A subject CAN be taught by 0 or more teachers.
A subject CAN have 0 or 1 room type constraint. If a subject does not have any
room type constraint, it is assumed to be require a general room.

# Teachers
A teacher MUST teach at least one subject.
A teacher MUST be available at least 2 hours during the week.

# Classes
A class CAN study 0 or more subjects.
A class ONLY studies a subject IF there is a teacher available for that subject
and whose availabilities fit into the availabilities of the class.
A class MUST have a priority for each subject it studies. This priority will
later determine which class receives a teacher for that subject first when
distributing teachers.
A class CAN have a favorite room.

# Rooms
A room CAN have a type. If it does not have a type it is assumed to be a general
room.
A room MUST have a capacity indicating how many students it can receive at a
time.
