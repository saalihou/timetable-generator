Meteor.methods({
  duplicateTimeslots: function (sourceSet, destSet) {
    var sourceSlots = Timeslots.find({parentSet: sourceSet});
    sourceSlots.forEach(function (slot) {
      delete slot._id;
      slot.parentSet = destSet;
      try {
        Timeslots.insert(slot);
      } catch (err) {
        // timing errors may arise, but we shouldnt stop the duplication
        // nevertheless
      }
    });
  }
});
